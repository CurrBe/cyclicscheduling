from gurobipy import *
from math import *

from data import read_data, FILEPATH, prob_instance
from block_generation import gen_blocks

# how long we let the code run for
TIME_LIM = 950

def solve_mip(filename):
    """
    Solve the Block MIP model given an instance file

    Parameters
    ----------
    filename : str
        Name of the instance file

    Returns
    -------
    XV : dict
        Dictionary containing solved values of variables

    obj_val : float
        Objective value of the solved model

    run_time : float
        Time taken for the model to be solved

    SE : list
        Set of extended shifts
    
    D : list
        Set of days in planning horizon
    
    num_days : int
        Number of days in a single period (e.g. a week) in the planning horizon
    
    shift_name : list
        String labels for shifts
    """    
    # extract data for scenario
    num_days, num_teams, num_shifts, dem, days_off, shift_name, \
    shift_start, shift_length, shift_blocks, work_blocks, banned_two, \
    banned_three = read_data(filename)

    # define relevant sets
    S = range(num_shifts)
    SE = range(num_shifts * 2)
    D = range(num_days * num_teams)

    # set of extended shifts- maps each shift to the list of shifts that can 
    # follow it
    extended = [[] for s in SE]

    for s in S:
        extended[s] = [ss for ss in S if ss != s]
        extended[s].append(s + num_shifts)
    
    for s in SE[num_shifts:]:
        extended[s] = [ss for ss in SE if ss != s]

    for seq in banned_two:
        extended[seq[0]].remove(seq[1])

    for seq in banned_three:
        extended[seq[0] + num_shifts].remove(seq[2])

    # generate the work blocks
    blocks, successors = gen_blocks(S, SE, D, shift_blocks, work_blocks, extended, banned_three)

    # reduce extended shifts to only contain one break block
    SE = range(num_shifts + 1)
    I = range(len(blocks))

    # 1 if shift s in position n of block i, 0 otherwise
    a = {(i, s, n): (1 if blocks[i][n] == s else 0)
            for i in I for s in SE for n in range(len(blocks[i]))}

    m = Model('Block Formulation Scheduling')

    # U[i, d] = 1 if place block i starting at day d, 0 otherwise
    U = {(i, d): m.addVar(vtype = GRB.BINARY) for i in I for d in D}

    # X[s, d] = 1 if shift s worked on day d, 0 otherwise
    X = {(s, d): m.addVar(vtype = GRB.BINARY) for s in SE for d in D}

    # Upper bound on number of times any given shift is worked in planning horizon
    Z = m.addVar()

    m.setObjective(Z, GRB.MINIMIZE)

    m.setParam('TimeLimit', TIME_LIM)


    # Z is upper bound on num times any shift worked in planning horizon
    ZUpperBound = {s: m.addConstr(quicksum(X[s, d] for d in D) <= Z) for s in S}
    
    # must meet the demand for each day of week (or work period)
    DemMet = {(s, d): m.addConstr(quicksum(X[s, t * num_days + d] for t in range(num_teams)) >= dem[s][d])
                for s in S for d in range(num_days)}
    
    # must either do a working shift or a break shift each day
    OneShiftPerDay = {d: m.addConstr(quicksum(X[s, d] for s in SE) == 1)
                        for d in D}

    # placing a block i will fix whatever shifts we work on for a length of time
    BlockShiftDependence = {(s, d): m.addConstr(quicksum(U[i, (d - n) % len(D)] * a[i, s, n]
                                for i in I for n in range(len(blocks[i]))) == X[s, d])
                    for (s, d) in X}

    # can't work any triple banned shift sequences
    NoBadCombos = {(tuple(t), d): m.addConstr(quicksum(X[t[i], (d + i) % len(D)] for i in range(3)) <= 2) for t in banned_three for d in D}
    

    m.optimize()

    ob_val = m.objVal
    run_time = m.Runtime

    XV = {(s, d): X[s, d].x for (s, d) in X}

    return XV, ob_val, run_time, SE, D, num_days, shift_name

def run_blocks(file_list, dest, show_log = False):
    """
    Run the block model for a set of instances

    Parameters
    ----------
    file_list : list
        List of instance file names
    dest : str
        File we save results into
    show_log : bool, optional
        Whether to show the Gurobi log, by default False
    """    

    if os.path.isfile(dest):
        os.remove(dest)
    for file in file_list:
        print('*' * 30)
        print(file)
        print('*' * 30)
        XV, ob_val, run_time, SE, D, num_days, shift_name = solve_mip(file)
        with open(dest, 'a') as f:
            line = ''
            line += 'Solution for {}\n'.format(file)
            line += '-' * 30 + '\n\n'
            line += 'Objective Value: {}\n'.format(round(ob_val, 3))
            line += 'Runtime: \t{}\n\n'.format(round(run_time, 3))

            for d in D:
                if run_time > 950:
                    line += '*' * 10 + ' TIMED OUT ' + '*' * 10 + '\n\n'
                    break
                for s in SE:
                    if XV[s, d] > 0.9:
                        line += ' {} '.format(shift_name[s])
                if d % num_days == num_days - 1:
                    line += '\n'
            line += '\n\n'

            f.writelines(line)


def main():
    test_files = os.listdir(os.path.join(FILEPATH, prob_instance))

    file_list = []

    for i in range(len(test_files)):
        if test_files[i][-4:] != '.txt':
            continue
        file_list.append(test_files[i][:-4])
    
    run_blocks(file_list, 'BlockFormResults.txt', show_log=True)
    

if __name__ == '__main__':
    main()
    
