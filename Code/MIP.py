from gurobipy import *
import os

from data import read_data, FILEPATH, prob_instance

TIME_LIM = 950

def MIP(filename, show_log = False):
    """
    Solve the base MIP model provided by Rocha et. al. (2014)

    Parameters
    ----------
    filename : str
        Name of the instance file containing data

    show_log : bool, optional
        Whether or not to show the Gurobi solve log, by default False

    Returns
    -------
    XV : dict
        Dictionary containing solved values of variables

    obj_val : float
        Objective value of the solved model

    run_time : float
        Time taken for the model to be solved

    T : list
        Set of teams

    SE : list
        Set of extended shifts
    
    D : list
        Set of days in planning horizon
    
    num_days : int
        Number of days in a single period (e.g. a week) in the planning horizon
    
    shift_name : list
        String labels for shifts
    """    

    XV, obj_val, run_time, T, SE, D, num_days, shift_name
    # extract data for scenario
    num_days, num_teams, num_shifts, dem, days_off, shift_name, \
    shift_start, shift_length, shift_blocks, work_blocks, banned_two, \
    banned_three = read_data(filename)

    # set of working shifts
    S = range(num_shifts)
    # set of extended shifts (includes working shifts and break shifts)
    SE = range(num_shifts * 2)
    # set of days in planning horizon
    D = range(num_days * num_teams)
    # set of teams
    T = range(num_teams)

    # offset between schedules of each team
    delta = num_days

    # list containing the extended shifts that can follow each extended shift
    # this will account for banned sequences
    extended = [[] for s in SE]

    for s in S:
        extended[s] = [ss for ss in S if ss != s]
        extended[s].append(s + num_shifts)
    
    for s in SE[num_shifts:]:
        extended[s] = [ss for ss in SE if ss != s]

    for seq in banned_two:
        extended[seq[0]].remove(seq[1])

    for seq in banned_three:
        extended[seq[0] + num_shifts].remove(seq[2])


    css = Model('Rotating Schedule')
    css.setParam('TimeLimit', TIME_LIM)

    # X[t, s, d] = 1 if team t works shift s on day d, 0 otherwise
    X = {(t, s, d): css.addVar(vtype = GRB.BINARY) for t in T for s in SE for d in D}

    # Y[t, d, m] = 1 if team t works a block of at least minD days starting on day
    # d + m - 1
    Y = {(t, d, m): css.addVar(vtype = GRB.BINARY) for t in T for d in D for m in range(1, work_blocks[0] + 1)}

    # upper bound on max number of times a team works a shift in planning horizon
    Z = css.addVar()

    css.setObjective(Z, GRB.MINIMIZE)

    # Z = min(max_{t, s}(sum(X[t, s, d] for  d in D))
    ZDepX = {(t, s): css.addConstr(quicksum(X[t, s, d] for d in D) - Z <= 0)
                for t in T for s in S}
    
    # Must work one extended shift each day
    OneShiftPerDay = {(t, d): css.addConstr(quicksum(X[t, s, d] for s in SE) == 1)
                        for t in T for d in D}

    # Must meet required demand
    DemandMet = {(s, d): css.addConstr(quicksum(X[t, s, d] for t in T) >= dem[s][d % num_days])
                    for s in S for d in D}

    # offset teams so their schedules are out of sync by delta dayss
    TeamOffset = {(t, s, d): css.addConstr(X[t, s, d] - X[(t + 1) % len(T), s, (d + delta) % len(D)] == 0)
                    for t in T for s in SE for d in D}

    # can't work a block of more than maxD days
    MaxWorkDays = {(t, d): 
                css.addConstr(quicksum(X[t, s, (d + q) % len(D)] for q in range(work_blocks[1] + 1) for s in S) <= work_blocks[1])
                    for t in T for d in D}

    # must work at least minD days
    MinWorkDays1 = {(t, d): css.addConstr(quicksum(Y[t, d, m] for m in range(1, work_blocks[0] + 1)) - \
                        quicksum(X[t, s, (d + work_blocks[0] - 1) % len(D)] for s in S) >= 0)
                        for t in T for d in D}

    MinWorkDays2 = {(t, d, m): css.addConstr(quicksum(X[t, s, (d + q - 1) % len(D)]
                                                for s in S for q in range(m, m + work_blocks[0])) - \
                                                work_blocks[0] * Y[t, d, m] >= 0)
                    for (t, d, m) in Y}
    
    # can only follow valid sequence of shifts (respect banned shifts)
    ExtendedShifts = {(t, s, d):
                        css.addConstr(X[t, s, d] - X[t, s, (d + 1) % len(D)] - \
                            quicksum(X[t, ss, (d + 1) % len(D)] for ss in extended[s]) <= 0)
                        for t in T for s in SE for d in D}

    # must work a minimum amount of days on a given working shift
    MinShiftBlockW = {(t, s, d): css.addConstr(
                        shift_blocks[s][0] * (X[t, s, d] - X[t, s, (d - 1) % len(D)]) <= \
                            quicksum(X[t, s, (d + q) % len(D)] for q in range(shift_blocks[s][0])))
                        for t in T for s in S for d in D}

    # must work a minimum amount of days on a break block
    MinShiftBlockB = {(t, s, d): css.addConstr(
                        shift_blocks[s][0] * (X[t, s, d] - \
                            quicksum(X[t, ss, (d - 1) % len(D)] for ss in SE[num_shifts:])) <= \
                            quicksum(X[t, ss, (d + q) % len(D)] for ss in SE[num_shifts:] for q in range(shift_blocks[s][0])))
                        for t in T for s in SE[num_shifts:] for d in D}
    
    # can't work more than a certain period in a row for a work shift
    MaxShiftBlockW = {(t, s, d): css.addConstr(quicksum(X[t, s, (d + q) % len(D)]
                                for q in range(shift_blocks[s][1] + 1)) <= shift_blocks[s][1])
                        for t in T for s in S for d in D}
    
    # can't work more than a certain period in a row for a break block
    MaxShiftBlockB = {(t, d): css.addConstr(quicksum(X[t, s, (d + q) % len(D)]
                                for q in range(shift_blocks[-1][1] + 1) 
                                for s in SE[num_shifts:]) <= shift_blocks[-1][1])
                        for t in T for d in D}


    if not show_log:
        css.setParam('OutputFlag', 0)
    else:
        css.setParam('OutputFlag', 1)

    css.optimize()

    run_time = css.Runtime

    obj_val = css.objVal

    if run_time >= TIME_LIM:
        XV = {}
    else:
        XV = {(t, s, d): X[t, s, d].x for (t, s, d) in X}
    
    return XV, obj_val, run_time, T, SE, D, num_days, shift_name

def run_MIP(file_list, dest, show_log = False):
    """
    Run the base MIP for a set of problem instances

    Parameters
    ----------
    file_list : list
        List of the instance files being solved

    dest : str
        Name of the file where results are saved
        
    show_log : bool, optional
        Whether or not to show the Gurobi solve log, by default False
    """    
    if os.path.isfile(dest):
        os.remove(dest)
    for file in file_list:
        print('*' * 30)
        print(file)
        print('*' * 30)
        XV, ob_val, run_time, T, SE, D, num_days, shift_name = MIP(file, \
                                                            show_log = show_log)
        with open(dest, 'a') as f:
            line = ''
            line += 'Solution for {}\n'.format(file)
            line += '-' * 30 + '\n\n'
            line += 'Objective Value: {}\n'.format(round(ob_val, 3))
            line += 'Runtime: \t{}\n\n'.format(round(run_time, 3))

            for d in D:
                if run_time > 950:
                    line += '*' * 10 + ' TIMED OUT ' + '*' * 10 + '\n\n'
                    break
                for s in SE:
                    if XV[0, s, d] > 0.9:
                        line += ' {} '.format(shift_name[s])
                if d % num_days == num_days - 1:
                    line += '\n'
            line += '\n\n'

            f.writelines(line)

def main():
    test_files = os.listdir(os.path.join(FILEPATH, prob_instance))

    file_list = []

    for i in range(len(test_files)):
        if test_files[i][-4:] != '.txt':
            continue
        file_list.append(test_files[i][:-4])
    
    dest = 'InitialMIPResults.txt'
    show_log = True

    run_MIP(file_list, dest, show_log = show_log)

if __name__ == '__main__':
    main()



    

    
