import os

FILEPATH = os.path.join('C:', '\\Users', 'CurrB', 'OneDrive', 'Documents', '2020 Honours', 'Rocha', 'Problem Instances')
prob_instance = "Instances 20"

def read_data(filename):
    """
    Read instance data from a given data file

    Parameters
    ----------
    filename : str
        Name of the file containing the problem data

    Returns
    -------
    num_days : int
        Length of planning period, usually 7 days

    num_teams : int
        Number of teams in the instance

    num_shifts : int
        Number of shifts in the instance

    dem : list
        Demand matrix, number of teams required in each shift of the planning 
        period

    shift_name : list
        List of string names of shifts

    shift_start : list
        Starting time for each shift

    shift_length: list
        Duration of each shift

    shift_blocks : list
        Minimum and maximum possible length for a stretch of each shift type

    work_blocks : list
        Minimum and maximum length of a work stretch

    banned_two : list
        List of banned sequences of shifts of length 2

    banned_three : list
        List of banned sequences of shifts of length 3

    Raises
    ------
    ValueError
        Data file could not be found at the given path
    """    
    # define file path
    data_file = os.path.join(FILEPATH, prob_instance, filename + '.txt')
    # check that the file exists
    if not os.path.isfile(data_file):
        raise ValueError('No Data File Found at %s' %data_file)
    
    # extract raw file contents
    with open(data_file, 'r') as f:
        file_list = []
        line = f.readline()
        while line:
            file_list.append(line)
            line = f.readline()
    
    num_days = int(file_list[1])
    num_teams = int(file_list[4])
    num_shifts = int(file_list[7])

    dem = []
    for i in range(10, 10 + num_shifts):
        row = file_list[i].strip().split()
        row_int = []
        for j in row:
            row_int.append(int(j))
        dem.append(row_int)

    shifts = range(num_shifts)

    shift_name = [None for s in range(num_shifts * 2)]
    shift_start = [0 for s in shifts]
    shift_length = [0 for s in shifts]
    shift_blocks = [[] for s in range(num_shifts * 2)]

    extended = []
    
    for s in shifts:
        shift_info = file_list[12 + num_shifts + s].strip().split()
        shift_name[s] = shift_info[0]
        shift_start[s] = int(shift_info[1])
        shift_length[s] = int(shift_info[2])
        shift_blocks[s] = [int(shift_info[3]), int(shift_info[4])]

        shift_name[-s - 1] = '-'
        extended.append(s + num_shifts)
    
    days_off = file_list[14 + 2 * num_shifts].strip().split()
    min_days_off = int(days_off[0])
    max_days_off = int(days_off[1])
    
    for s in shifts:
        shift_blocks[-s - 1] = [min_days_off, max_days_off]

    work_blocks = file_list[17 + 2 * num_shifts].strip().split()
    min_work_block = int(work_blocks[0])
    max_work_block = int(work_blocks[1])
    work_blocks = [min_work_block, max_work_block]

    banned_seq_len = file_list[20 + 2 * num_shifts].strip().split()
    for i in range(len(banned_seq_len)):
        banned_seq_len[i] = int(banned_seq_len[i])
    
    banned_two = []
    for i in range(23 + 2 * num_shifts, 23 + 2 * num_shifts + banned_seq_len[0]):
        seq = file_list[i].strip().split()
        index_seq = []
        for s in seq:
            shift_index = shift_name.index(s)
            index_seq.append(shift_index)
        banned_two.append(index_seq)
    
    banned_three = []
    for i in range(23 + 2 * num_shifts + banned_seq_len[0], 23 + 2 * num_shifts + banned_seq_len[0] + banned_seq_len[1]):
        seq = file_list[i].strip().split()
        index_seq = []
        for s in seq:
            shift_index = shift_name.index(s)
            index_seq.append(shift_index)
        banned_three.append(index_seq)

    return num_days, num_teams, num_shifts, dem, shift_name, \
        shift_start, shift_length, shift_blocks, work_blocks, banned_two, banned_three



def main():
    num_days, num_teams, num_shifts, dem, shift_name, \
    shift_start, shift_length, shift_blocks, work_blocks, banned_two, banned_three = read_data('Example5')


if __name__ == '__main__':
    main()