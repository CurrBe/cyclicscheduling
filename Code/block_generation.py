from data import read_data

def gen_blocks(S, SE, D, shift_blocks, work_blocks, extended, banned_triple):
    """
    Generate the set of work blocks

    Parameters
    ----------
    S : list
        Set of shifts

    SE : list
        Extended set of shifts, containing |S| working shifts and |S| break shifts

    D : list
        Set of days in planning horizon

    shift_blocks : list
        Minimum and maximum number of consecutive assignments for each shift

    work_blocks : list
        Minimum and maximum number of consecutive working days

    extended : list
        Set of shift extensions that can proceed each shift

    banned_triple : list
        Set of banned triple sequences of shifts

    Returns
    -------
    legal_blocks : list
        Set of blocks in the feasible region

    successors : dict
        Dictionary mapping each block to the set of blocks that can follow it
    """    
    # base set of blocks that we build upon to generate the full set
    init_blocks = {(s, n): [] for s in S for n in range(min(shift_blocks[s][0], work_blocks[0]), work_blocks[1] + 1)}
    # set of feasible blocks of working shifts
    legal_work_blocks = {(s, n): [] for s in S for n in range(min(shift_blocks[s][0], work_blocks[0]), work_blocks[1] + 1)}
    # set of feasible blocks
    legal_blocks = []
    for s in S:
        init_blocks[s, shift_blocks[s][0]].append([s] * shift_blocks[s][0])
        for n in range(shift_blocks[s][0], work_blocks[1]):   
            for block in init_blocks[s, n]:
                new_blocks = []
                counts = []
                prev_shift = block[-1]
                count = 1
                for i in range(2, n + 1):
                    if block[-i] == prev_shift:
                        count += 1
                    else:
                        break
                if count < shift_blocks[prev_shift][0]:
                    new_block = block + [prev_shift]
                    init_blocks[s, n + 1].append(new_block)
                    count += 1
                    new_blocks.append(new_block)
                    counts.append(count)
                else:
                    choices = extended[prev_shift] + [prev_shift]
                    for ss in choices:
                        new_count = count
                        if ss in SE[len(S):]:
                            continue
                        elif ss == prev_shift:
                            if new_count >= shift_blocks[prev_shift][1]:
                                continue
                            new_count += 1
                            counts.append(new_count)
                        else:
                            new_count = 1
                            counts.append(new_count)
                        new_block = block + [ss]
                        init_blocks[s, n + 1].append(new_block)
                        new_blocks.append(new_block)
                if n >= work_blocks[0] - 1:
                    for i in range(len(new_blocks)):
                        if counts[i] >= shift_blocks[new_blocks[i][-1]][0]:
                            legal_work_blocks[s, n + 1].append(new_blocks[i])
                            for j in range(shift_blocks[-1][0], shift_blocks[-1][1] + 1):
                                legal_blocks.append(tuple(new_blocks[i] + [len(S)] * j))
    
    # one block can only immediately succeed another if they don't form a banned
    # triple
    successors = {i: [] for i in range(len(legal_blocks))}
    if len(banned_triple) >= 1:
        for i in range(len(legal_blocks)):
            for j in range(len(legal_blocks)):
                if not any([
                    legal_blocks[i][-2] == t[0] and 
                    legal_blocks[i][-1] == t[1] and 
                    legal_blocks[j][0] == t[2] for t in banned_triple
                ]):
                    successors[i].append(j)
    else:
        for i in range(len(legal_blocks)):
            successors[i] = [j for j in range(len(legal_blocks))]
    
    return legal_blocks, successors


def main():
    num_days, num_teams, num_shifts, dem, days_off, shift_name, \
    shift_start, shift_length, shift_blocks, work_blocks, banned_two, \
    banned_three= read_data('Example1')

    print('BT: ', banned_three)

    # print(shift_blocks)
    #set of working shifts
    S = range(num_shifts)
    #set of extended shifts (includes working shifts and break shifts)
    SE = range(num_shifts * 2)
    #set of days in planning horizon
    D = range(num_days * num_teams)
    #set of teams
    T = range(num_teams)

    #offset between schedules of each team
    delta = num_days

    #list containing the extended shifts that can follow each extended shift
    #this will account for banned sequences
    extended = [[] for s in SE]

    for s in S:
        extended[s] = [ss for ss in S if ss != s]
        extended[s].append(s + num_shifts)
    
    for s in SE[num_shifts:]:
        extended[s] = [ss for ss in SE if ss != s]

    for seq in banned_two:
        extended[seq[0]].remove(seq[1])

    for seq in banned_three:
        extended[seq[0] + num_shifts].remove(seq[2])

    blocks, successors = gen_blocks(S, SE, D, shift_blocks, work_blocks, extended, banned_three)

    for i in blocks:
        print('B: {}\t\tL: {}'.format(i, len(successors[i])))

if __name__ == '__main__':
    main()



    

